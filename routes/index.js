var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


/*
router.post('/rfid', function(req, res){
    res.status(200).send('Hola desde java! (post)');
    console.log(req.datos);
});
*/

/*
var arrayMensajes = [{
    id: 1,
    text: 'Hola soy un mensaje',
    author: 'Alan'
}];

oi.on('connection', function(socket) {
    console.log('Conexión de socket');

    socket.emit('messages', arrayMensajes);

    socket.on('mensajeDelCliente', function(data){
        arrayMensajes.push(data);
        console.log(data);
        io.sockets.emit('messages', arrayMensajes);
    });
});
*/
module.exports = router;
