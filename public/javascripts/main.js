/**
 * Created by locobit on 12/06/17.
 */

var socket = io.connect('http://localhost:3000', { 'forceNew':true });

socket.on('totales', function(data){
    console.log(data);
    mostrarTotales(data);
});

function mostrarTotales(data) {
    var html = `<h1>en Almacen: ${data.enAlmacen}</h1>
        <br><h1>en Ruta: ${data.enRuta}</h1>
        <br><h1>en Destino: ${data.enDestino}</h1>`;

    document.getElementById('divTotales').innerHTML = html;

    Highcharts.chart('containerBarras', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Control de Inventario de Camiones'
        },
        xAxis: {
            categories: ['Camiones'],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Cantidad',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' camiones'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                },
                animation: false
            }
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 40,
            y: 80,
            floating: false,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Total',
            data: [data.enAlmacen + data.enDestino + data.enRuta]
        }, {
            name: 'En Almacén',
            data: [data.enAlmacen],
            color: '#F50606'
        }, {
            name: 'En Ruta',
            data: [data.enRuta],
            color: '#3606F5'
        }, {
            name: 'En Destino',
            data: [data.enDestino],
            color: '#06F58A'
        }]
    });

    Highcharts.chart('containerPie', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Control de Inventario de Camiones'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true
                },
                showInLegend: true,
                animation: false
            }
        },
        series: [{
            name: 'Camiones',
            colorByPoint: true,
            data: [{
                name: 'En Almacén',
                y: data.enAlmacen,
                color: '#F50606'
            }, {
                name: 'En Ruta',
                y: data.enRuta,
                color: '#3606F5',
                sliced: true,
                selected: true
            }, {
                name: 'En Destino',
                y: data.enDestino,
                color: '#06F58A'
            }]
        }]
    });
}

